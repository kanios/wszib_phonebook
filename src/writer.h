struct pb_entry {
    char name[32];
    char number[32];
};

void add(struct pb_entry entry);
void del(char with[]);
void edit(char decision, char with[]);

void add(struct pb_entry entry) {
    if (record_exists(entry.name) || record_exists(entry.number)) {
        printf("Record already exists!\n");
        return;
    }

    FILE* stream = fopen(PB_PATH, "a");
    char newline[256] = {0};

    snprintf(newline, sizeof(newline), "%s;%s\n", entry.name, entry.number);
    fputs(newline, stream);
    fclose(stream);

    printf("%s (%s) added successfully!", entry.name, entry.number);
}

void del(char with[]) {
    FILE* inf = fopen(PB_PATH, "r");
    FILE* outf = fopen(TMP_PATH, "w");
    char line[256];
    int deleted = 0;

    while (fgets(line, 256, inf)) {
        char name[64];
        char number[64];
        int name_l = 0;
        size_t size = sizeof(line) / sizeof(line[0]);

        for (int i = 0; i < size; i++) {
            if (line[i] != ';') {
                name[i] = line[i];
            } else {
                name[i] = '\0';
                name_l = i;
                break;
            }
        }

        for (int i = name_l + 1; i < size - 2; i++) {
            if (line[i] != '\n') {
                number[i - name_l - 1] = line[i];
            } else {
                number[i - name_l - 1] = '\0';
                break;
            }
        }

        if (strcmp(name, with) != 0 && strcmp(number, with) != 0) {
            fprintf(outf, "%s", line);
        } else {
            deleted++;
        }
    }

    if (deleted > 0) {
        printf("Deleted %d record/-s succesfully!\n", deleted);
    } else {
        printf("Provided record does not exist in your phonebook!\n");
    }

    fclose(inf);
    fclose(outf);

    remove(PB_PATH);
    rename(TMP_PATH, PB_PATH);
}

void edit(char decision, char with[]) {
    FILE* inf = fopen(PB_PATH, "r");
    FILE* outf = fopen(TMP_PATH, "w");
    char line[256];
    char new_entry[32];

    if (decision == 'N') {
        printf("Provide new name:\n");
        printf("\n> ");
    } else {
        printf("Provide new number:\n");
        printf("\n> ");
    }

    scanf(" %s", &new_entry);

    if (record_exists(new_entry)) {
        printf("Record (%s) already exists!\n", new_entry);
        return;
    }

    while (fgets(line, 256, inf)) {
        char name[64];
        char number[64];
        int name_l = 0;
        size_t size = sizeof(line) / sizeof(line[0]);

        for (int i = 0; i < size; i++) {
            if (line[i] != ';') {
                name[i] = line[i];
            } else {
                name[i] = '\0';
                name_l = i;
                break;
            }
        }

        for (int i = name_l + 1; i < size - 2; i++) {
            if (line[i] != '\n') {
                number[i - name_l - 1] = line[i];
            } else {
                number[i - name_l - 1] = '\0';
                break;
            }
        }

        if (strcmp(name, with) != 0 && strcmp(number, with) != 0) {
            fprintf(outf, "%s", line);
        } else if (strcmp(name, with) == 0 || strcmp(number, with) == 0) {
            if (decision == 'N') {
                fprintf(outf, "%s;%s\n", new_entry, number);
            } else if (decision == 'O') {
                fprintf(outf, "%s;%s\n", name, new_entry);
            }
        }
    }

    fclose(inf);
    fclose(outf);

    printf("Record edited succesfully!\n");

    remove(PB_PATH);
    rename(TMP_PATH, PB_PATH);
}
