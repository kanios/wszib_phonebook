#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

const char* get_field(char* line, int num);

void show_all();
void show_exact(char record[]);
void show_like(char with[]);

const char* get_field(char* line, int num) {
    const char* tok;

    for (tok = strtok(line, ";"); tok && *tok; tok = strtok(NULL, ";\n")) {
        if (!--num) {
            return tok;
        }
    }

    return NULL;
}

void show_all() {
    FILE* stream = fopen(PB_PATH, "r");
    char line[256];

    printf("NAME\t\tNUMBER\n");

    while (fgets(line, 256, stream)) {
        char *tmp = strdup(line);

        printf("%s\t\t%s\n", get_field(tmp, 1), get_field(tmp, 2));
        free(tmp);
    }

    fclose(stream);
}

void show_exact(char record[]) {
    FILE* stream = fopen(PB_PATH, "r");
    char line[256];

    while (fgets(line, 256, stream)) {
        char name[64];
        char number[64];
        int name_l = 0;
        size_t size = sizeof(line) / sizeof(line[0]);

        for (int i = 0; i < size; i++) {
            if (line[i] != ';') {
                name[i] = line[i];
            } else {
                name[i] = '\0';
                name_l = i;
                break;
            }
        }

        for (int i = name_l + 1; i < size - 2; i++) {
            if (line[i] != '\n') {
                number[i - name_l - 1] = line[i];
            } else {
                number[i - name_l - 1] = '\0';
                break;
            }
        }

        if (strcmp(name, record) == 0 || strcmp(number, record) == 0) {
            printf("%s\t\t%s\n", name, number);
        }
    }

    fclose(stream);
}

void show_like(char with[]) {
    FILE* stream = fopen(PB_PATH, "r");
    char line[256];

    printf("NAME\t\tNUMBER\n");

    while (fgets(line, 256, stream)) {
        if (strstr(line, with) != NULL) {
            printf("%s\t\t%s\n", get_field(line, 1), get_field(line, 2));
        }
    }
}
