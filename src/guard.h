#include <stdio.h>
#include <memory.h>

int record_exists(char rec[]);

int record_exists(char rec[]) {
    FILE* stream = fopen(PB_PATH, "r");
    char line[256];

    while (fgets(line, 256, stream)) {
        char name[64];
        char number[64];
        int name_l = 0;
        size_t size = sizeof(line) / sizeof(line[0]);

        for (int i = 0; i < size; i++) {
            if (line[i] != ';') {
                name[i] = line[i];
            } else {
                name[i] = '\0';
                name_l = i;
                break;
            }
        }

        for (int i = name_l + 1; i < size - 2; i++) {
            if (line[i] != '\n') {
                number[i - name_l - 1] = line[i];
            } else {
                number[i - name_l - 1] = '\0';
                break;
            }
        }

        if (strcmp(name, rec) == 0 || strcmp(number, rec) == 0) {
            return 1;
        }
    }

    return 0;
}
