#include <stdio.h>

struct person {
    char name[32];
    char number[32];
};

int csv_to_arr(struct person* result);
static int my_cmp_a(const void *a, const void *b);
static int my_cmp_d(const void *a, const void *b);
void sort_asc(const char *arr[], int n);
void sort_desc(const char *arr[], int n);
void print_sorted(char by, char v);

int csv_to_arr(struct person* result) {
    FILE* stream = fopen(PB_PATH, "r");
    char line[256];
    int iter = 0;

    while (fgets(line, 256, stream)) {
        int name_l = 0;
        size_t size = sizeof(line) / sizeof(line[0]);

        for (int i = 0; i < size; i++) {
            if (line[i] != ';') {
                result[iter].name[i] = line[i];
            } else {
                result[iter].name[i] = '\0';
                name_l = i;
                break;
            }
        }

        for (int i = name_l + 1; i < size - 2; i++) {
            if (line[i] != '\n') {
                result[iter].number[i - name_l - 1] = line[i];
            } else {
                result[iter].number[i - name_l - 1] = '\0';
                break;
            }
        }

        iter++;
    }

    return iter;
}

static int my_cmp_a (const void *a, const void *b) {
    return strcmp(*(const char **) a, *(const char **) b);
}


static int my_cmp_d (const void *a, const void *b) {
    return -strcmp(*(const char **) a, *(const char **) b);
}

void sort_asc(const char *arr[], int n) {
    qsort(arr, n, sizeof(const char *), my_cmp_a);
}

void sort_desc(const char *arr[], int n) {
    qsort(arr, n, sizeof(const char *), my_cmp_d);
}

void print_sorted(char by, char v) {
    struct person ppl[256];
    int ppl_size = csv_to_arr(&ppl);
    const char *str_arr[] = {};

    printf("NAME\t\tNUMBER\n");

    for (int i = 0; i < ppl_size; i++) {
        switch (by) {
            case 'N':
                str_arr[i] = ppl[i].name;
                break;
            case 'O':
                str_arr[i] = ppl[i].number;
                break;
            default:
                break;
        }
    }

    switch (v) {
        case 'A':
            sort_asc(str_arr, ppl_size);
            break;
        case 'D':
            sort_desc(str_arr, ppl_size);
            break;
        default:
            break;
    }

    for (int i = 0; i < ppl_size; i++) {
        show_exact(str_arr[i]);
    }
}
