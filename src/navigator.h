#include <stdio.h>
#include <stdlib.h>

#include "reader.h"
#include "writer.h"
#include "sorter.h"
#include "guard.h"

void flow();
void welcome();
void clear();
void menu();
void goodbye();
void ptc();

void show_menu();
void show_navigate();
void show_switch(char decision);

void add_menu();
void del_menu();

void edit_navigate();
void edit_menu(char rec[]);

void sort_navigate(char by);

void switcher(char decision);

void flow() {
    char decision;

    clear();
    menu();
    printf("\n> ");
    scanf(" %c", &decision);
    switcher(decision);
    goodbye();
}

void welcome() {
    clear();
    printf("Welcome to your Hermes Phonebook\n");
}

void menu() {
    printf("Please choose an action:\n");
    printf("\tS - show record/-s\n");
    printf("\tA - add new record\n");
    printf("\tD - delete record/-s\n");
    printf("\tE - edit existing record\n");
    printf("\tX - exit\n");
}

void clear() {
    system("clear");
}

void goodbye() {
    clear();
    printf("Hermes is closing...\n");
    printf("I hope to see you soon!\n");
    exit(1);
}

void show_menu() {
    printf("Please choose action for show:\n");
    printf("\t* - all\n");
    printf("\tD - all descended\n");
    printf("\tA - all ascended\n");
    printf("\tL - all like typed string\n");
    printf("\tE - exact record\n");
    printf("\tB - go back\n");
    printf("\tX - exit\n");
}

void show_navigate() {
    char decision;

    clear();
    show_menu();
    printf("\n> ");
    scanf(" %c", &decision);
    show_switch(decision);
}

void sort_navigate(char by) {
    char decision;

    clear();
    printf("Sort by:\n");
    printf("\tN - name\n");
    printf("\tO - number\n");
    printf("\n> ");
    scanf(" %s", &decision);
    clear();
    print_sorted(decision, by);
}

void show_switch(char decision) {
    char rec[32];

    clear();

    switch (decision) {
        case '*':
            show_all();
            ptc();
            break;
        case 'D':
            sort_navigate('D');
            ptc();
            break;
        case 'A':
            sort_navigate('A');
            ptc();
            break;
        case 'L':
            printf("Type string:\n");
            printf("\n> ");
            scanf(" %s", &rec);
            clear();
            show_like(rec);
            ptc();
            break;
        case 'E':
            printf("Type name or number:\n");
            printf("\n> ");
            scanf(" %s", &rec);
            clear();
            printf("NAME\t\tNUMBER\n");
            show_exact(rec);
            ptc();
            break;
        case 'B':
            flow();
            break;
        case 'X':
            goodbye();
            break;
        default:
            clear();
            printf("I do not recognize this show action. Could you repeat?\n");
    }

    show_navigate();
}

void add_menu() {
    struct pb_entry entry = {};

    printf("Type name:\n");
    printf("\n> ");
    scanf(" %s", &entry.name);
    printf("Type number:\n");
    printf("\n> ");
    scanf(" %s", &entry.number);
    clear();
    add(entry);
    ptc();
}

void del_menu() {
    char rec[32];

    printf("Type name or number:\n");
    printf("\n> ");
    scanf(" %s", &rec);
    clear();
    del(rec);
    ptc();
}

void edit_navigate() {
    char rec[32];

    printf("Type name or number:\n");
    printf("\n> ");

    scanf(" %s", &rec);
    clear();

    if (record_exists(rec)) {
        edit_menu(rec);
    } else {
        printf("Record (%s) does not exist! Would you like to add new one? Select 'A' from main menu.\n", rec);
    }

    ptc();
}

void edit_menu(char rec[]) {
    char decision;

    printf("NAME\t\tNUMBER\n");
    show_exact(rec);

    printf("You are about to edit the record above. Which part do you want to change?\n");
    printf("\tN - Name\n");
    printf("\tO - Number\n");
    printf("\n> ");

    scanf(" %c", &decision);
    edit(decision, rec);
}

void switcher(char decision) {
    clear();

    switch(decision) {
        case 'A':
            add_menu();
            break;
        case 'D':
            del_menu();
            break;
        case 'E':
            edit_navigate();
            break;
        case 'S':
            show_navigate();
            break;
        case 'X':
            goodbye();
            break;
        default:
            printf("I do not recognize this action. Could you repeat?\n");
    }

    flow();
}

void ptc() {
    char ch;

    printf("\nPress any key to continue...");
    scanf(" %c", &ch);
}
