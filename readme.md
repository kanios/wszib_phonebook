### Hermes
##### How to start

###### BEFORE COMPILING CHANGE PATHS OF YOUR PB.CSV AND TMP.CSV FILES IN MAIN.C FILE

There are few ways to start _Hermes Phonebook_:

* You might want to use CLion by JetBrains with _cc_ compiler and just click Build&Run
* You can type `gcc main.c` in your terminal and run _a.out_ file
